@echo off
setlocal

set arch=%1
set config=%2
set stage=%3
set make_cmd=%4

REM Yeah, I know. Batch files suck & I suck at batch files.
set make_arg_0=%5
set make_arg_1=%6
set make_arg_2=%7
set make_arg_3=%8
set make_arg_4=%9

REM needs to find zlib1.dll
set PATH=%PATH%;%stage%/bin

:setenv
call "c:\Program Files (x86)\Microsoft Visual Studio 10.0\VC\vcvarsall.bat" %arch%

REM FIXME: Find out how to have Qt find our custom-built libraries.
REM -system-libpng
REM -system-libjpeg
REM -no-qt3support           
REM -no-xmlpatterns          
REM -no-multimedia           
REM -no-phonon               
REM -nomake tools            
REM -nomake examples         
REM -nomake demos            

REM -qt-zlib to make it work right now, and disable zlib in mansdk.

REM QT really wants a zdll.lib file, make it happy.
cp %stage%/lib/zlib.lib %stage%/lib/zdll.lib

:configure
call "%~dp0configure.bat"    ^
	-opengl desktop          ^
    -platform win32-msvc2010 ^
    -opensource              ^
    -openssl                 ^
    -openssl-linked          ^
	-confirm-license         ^
    -%config%                ^
	-mp                      ^
	-system-zlib             ^
	-nomake examples         ^
    -nomake demos            ^
    -I %stage%/include       ^
    -L %stage%/lib

if errorlevel 1 goto abort

:make
%make_cmd% %make_arg_0% %make_arg_1% %make_arg_2% %make_arg_3% %make_arg_4%
if errorlevel 1 goto abort

goto end
:abort
exit /b 1
:end
